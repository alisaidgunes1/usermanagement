package services

import (
	"alisaidgunes.com/usermanagement/models"
)

type UserService interface {
	CreateUser(*models.User) error
	GetUser(string) (*models.User, error)
	GetAll() ([]*models.User, error)
	DeleteUser(string) error
	UpdateUser(*models.User, string) error
}
