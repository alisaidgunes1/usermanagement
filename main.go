package main

import (
	"alisaidgunes.com/usermanagement/controllers"
	"alisaidgunes.com/usermanagement/services"
	"context"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"os"
)

var (
	server      *gin.Engine
	us          services.UserService
	uc          controllers.UserController
	ctx         context.Context
	userc       *mongo.Collection
	mongoclient *mongo.Client
	err         error
)

func init() {
	godotenv.Load()

	dbUri := os.Getenv("DB_URI")

	ctx = context.TODO()

	mongoconn := options.Client().ApplyURI(dbUri)
	mongoclient, err = mongo.Connect(ctx, mongoconn)
	if err != nil {
		log.Fatal("error while connecting with mongo", err)
	}
	err = mongoclient.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal("error while trying to ping mongo", err)
	}

	fmt.Println("mongo connection established")

	userc = mongoclient.Database("userdb").Collection("users")
	us = services.NewUserService(userc, ctx)
	uc = controllers.New(us)
	server = gin.Default()
	server.Use(cors.Default())
}

func main() {

	port := os.Getenv("PORT")

	if port == "" {
		port = "9090"
	}
	defer mongoclient.Disconnect(ctx)

	basepath := server.Group("/v1")

	uc.RegisterUserRoutes(basepath)

	log.Fatal(server.Run(":" + port))

}
